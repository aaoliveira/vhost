#Vhost
Version: 1.0

This script was created to manage the virtual host files from Apache server.

*It was only tested in Ubuntu system.*

#Installation

* Download the script.
* Create a Symlink by using the command `sudo ln -s /path/script /usr/bin/vhost`
* That's it!

#Documentation

##Usage

`$ vhost [options]`

##Options
####Add/Create a host in an interactive way
`vhost -a` or `vhost --add`

####Delete a host configuration
`vhost -d <filename>` or `vhost -r <filename>` or `vhost --delete <filename>` or `vhost --remove <filename>`

####View created hosts
`vhost -l` or `vhost --list`

#### View the script help
`vhost -h` or `vhost --help`

---

##Enjoy it!

#####Thanks for downloading this script! I hope it might help you and let me know if it is useful to you by sending an email to [diogocavilha@gmail.com](mailto:diogocavilha@gmail.com)